# Banking

Banking application for small screens


## Introduction
This is a FinTS banking application using aqbanking as backend library. So it
does support most common methods to do online banking. This application is able
to connect to your bank and will show your current balance and transactions.
It has been created for my upcoming Librem 5 smartphone by Purism.

Software is available as a Flatpak, see Gitlab page.