/* banking-backend.h
 *
 * Copyright 2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef void BankingUser;
typedef void BankingAccount;
typedef struct _BankingTransaction BankingTransaction;

#define BANKING_TYPE_BACKEND (banking_backend_get_type())

G_DECLARE_FINAL_TYPE (BankingBackend, banking_backend, BANKING, BACKEND, GObject)

BankingBackend *banking_backend_new (void);
gint banking_backend_get_num_accounts (BankingBackend *self);
GList *banking_backend_get_accounts (BankingBackend *self);
BankingUser *banking_backend_create_user (BankingBackend *self, const gchar *bank_code, const gchar *account);

void banking_user_get_sysid (BankingBackend *self, BankingUser *user);
void banking_user_get_accounts (BankingBackend *self, BankingUser *user);

void banking_user_load_balance (BankingBackend *self, BankingAccount *account);

const gchar *banking_account_get_name (BankingAccount *self);
const gchar *banking_account_get_number (BankingAccount *self);
gdouble banking_banking_get_account_balance (BankingBackend *self, BankingAccount *account);

GList *banking_account_get_transactions (BankingBackend *self, BankingAccount *account);

const gchar *banking_transaction_get_remote_name (BankingTransaction *self);
const gchar *banking_transaction_get_purpose (BankingTransaction *self);
const gchar *banking_transaction_get_date (BankingTransaction *self);
const gchar *banking_transaction_get_transaction_text (BankingTransaction *self);
gdouble banking_transaction_get_value (BankingTransaction *self);

G_END_DECLS
