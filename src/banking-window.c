/* banking-window.c
 *
 * Copyright 2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define HANDY_USE_UNSTABLE_API
#include <libhandy-0.0/handy.h>

#include "banking-assistant.h"
#include "banking-config.h"
#include "banking-window.h"
#include "banking-backend.h"

struct _BankingWindow {
  GtkApplicationWindow parent_instance;

  HdyLeaflet          *header_box;
  HdyLeaflet          *content_box;
  GtkWidget           *header_bar;
  GtkWidget           *sub_header_bar;
  GtkWidget           *back_button;
  GtkWidget           *search_button;
  HdyHeaderGroup      *header_group;

  GtkLabel            *label;
  GtkWidget           *account_listbox;
  GtkWidget           *transaction_listbox;
  GtkWidget           *bank_entry;
  GtkEntryCompletion  *bank_completion;
  GtkWidget           *account_number;
  GtkWidget           *account_password_entry;
  BankingBackend      *backend;
  BankingUser         *user;
  BankingAccount      *account;
  gchar               *user_name;
  gchar               *bank_code;
  gchar               *account_name;
  gchar               *account_password;
  gint                 hbci_version;
  gint                 http_major;
  gint                 http_minor;

  GList               *transactions;
  gint                 num_fetch;
  guint                sorter_source;
};

G_DEFINE_TYPE (BankingWindow, banking_window, GTK_TYPE_APPLICATION_WINDOW)

#define NUM_FETCH_LIMIT 20

enum {
  PROP_0,
  PROP_BACKEND,
  LAST_PROP,
};

static GParamSpec *obj_properties[LAST_PROP];


static void
banking_window_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  BankingWindow *self = BANKING_WINDOW (object);

  switch (prop_id) {
    case PROP_BACKEND: {
      self->backend = g_value_get_object (value);
    }
    break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
banking_window_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  BankingWindow *self = BANKING_WINDOW (object);

  switch (prop_id) {
    case PROP_BACKEND: {
      g_value_set_object (value, self->backend);
    }
    break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
update (BankingWindow *self)
{
  GtkWidget *header_child = hdy_leaflet_get_visible_child (self->header_box);
  HdyFold fold = hdy_leaflet_get_fold (self->header_box);

  g_assert (header_child == NULL || GTK_IS_HEADER_BAR (header_child));

  hdy_header_group_set_focus (self->header_group, fold == HDY_FOLD_FOLDED ? GTK_HEADER_BAR (header_child) : NULL);
}

static void
on_notify_header_visible_child (GObject       *sender,
                                GParamSpec    *pspec,
                                BankingWindow *self)
{
  update (self);
}

static void
on_notify_fold (GObject       *sender,
                GParamSpec    *pspec,
                BankingWindow *self)
{
  update (self);
}

static void
on_notify_visible_child (GObject       *sender,
                         GParamSpec    *pspec,
                         BankingWindow *self)
{
  hdy_leaflet_set_visible_child_name (self->content_box, "detail");
}

static void
on_back_button_clicked (GObject       *sender,
                        BankingWindow *self)
{
  hdy_leaflet_set_visible_child_name (self->content_box, "master");
}


static gboolean
load_accounts (gpointer user_data)
{
  BankingWindow *self = BANKING_WINDOW (user_data);
  GList *accounts;
  GList *list;
  g_autofree gchar *tmp = NULL;
  gdouble sum = 0.0f;

  accounts = banking_backend_get_accounts (self->backend);

  for (list = accounts; list != NULL; list = list->next) {
    GtkWidget *child;
    GtkWidget *type;
    GtkWidget *iban;
    GdkPixbuf *pixbuf;
    GtkWidget *logo;
    GtkWidget *balanced;
    BankingAccount *account = list->data;

    child = gtk_grid_new ();
    gtk_widget_set_hexpand(child, FALSE);
    gtk_widget_set_hexpand_set (child, TRUE);
    gtk_grid_set_row_spacing (GTK_GRID (child), 6);
    gtk_grid_set_column_spacing (GTK_GRID (child), 12);
    gtk_grid_set_row_homogeneous (GTK_GRID (child), TRUE);
    gtk_container_set_border_width (GTK_CONTAINER (child), 6);

    type = gtk_label_new (banking_account_get_name (account));
    gtk_label_set_line_wrap_mode (GTK_LABEL (type), PANGO_WRAP_WORD);
    gtk_label_set_line_wrap (GTK_LABEL (type), TRUE);
    gtk_widget_set_halign (type, GTK_ALIGN_START);

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);
    gtk_widget_set_visible (box, TRUE);
    gtk_grid_attach (GTK_GRID (child), box, 1, 1, 1, 1);

    tmp = g_strdup_printf ("<small>%s</small>", banking_account_get_number (account));
    iban = gtk_label_new ("");
    gtk_label_set_markup (GTK_LABEL (iban), tmp);
    gtk_label_set_line_wrap (GTK_LABEL (iban), TRUE);
    gtk_widget_set_sensitive (iban, FALSE);
    gtk_widget_set_hexpand(iban, TRUE);
    gtk_label_set_line_wrap_mode (GTK_LABEL (iban), PANGO_WRAP_WORD);
    gtk_widget_set_halign (iban, GTK_ALIGN_START);
    gtk_box_pack_start (GTK_BOX (box), iban, TRUE, TRUE, 0);

    pixbuf = gdk_pixbuf_new_from_resource_at_scale ("/org/tabos/banking/dsgv.svg", 32, 32, TRUE, NULL);
    logo = gtk_image_new_from_pixbuf (pixbuf);
    gtk_image_set_pixel_size (GTK_IMAGE (logo), 48);
    gdouble balance = banking_banking_get_account_balance (self->backend, account);
    tmp = g_strdup_printf ("<b>%.2f €</b>", balance);

    sum += balance;

    balanced = gtk_label_new ("");
    gtk_label_set_markup (GTK_LABEL (balanced), tmp);
    gtk_label_set_xalign (GTK_LABEL (balanced), 1.0);
    gtk_box_pack_end (GTK_BOX (box), balanced, TRUE, TRUE, 0);

    gtk_grid_attach (GTK_GRID (child), logo, 0, 0, 1, 2);
    gtk_grid_attach (GTK_GRID (child), type, 1, 0, 1, 1);
    /*gtk_grid_attach(GTK_GRID(child), iban, 1, 1, 1, 1); */
    /*gtk_grid_attach(GTK_GRID(child), balanced, 2, 1, 1, 1); */

    gtk_widget_show_all (child);
    g_object_set_data (G_OBJECT (child), "account", account);
    gtk_list_box_insert (GTK_LIST_BOX (self->account_listbox), child, -1);
  }

  tmp = g_strdup_printf ("%.2f EUR", sum);
  gtk_header_bar_set_subtitle (GTK_HEADER_BAR (self->header_bar), tmp);

  return G_SOURCE_REMOVE;
}

void
banking_window_load_accounts (BankingWindow *self)
{
  g_idle_add (load_accounts, self);
}


static void
on_login_button_clicked (GObject       *sender,
                         BankingWindow *self)
{
}

static void
clear_listbox (GtkWidget *listbox)
{
  GList *children, *iter;

  children = gtk_container_get_children (GTK_CONTAINER (listbox));
  for (iter = children; iter != NULL; iter = g_list_next (iter)) {
    gtk_widget_destroy (GTK_WIDGET (iter->data));
  }
  g_list_free (children);
}


void
box_header_func (GtkListBoxRow *row,
                 GtkListBoxRow *before,
                 gpointer       user_data)
{
  GtkWidget *current;

  if (!before) {
    gtk_list_box_row_set_header (row, NULL);
    return;
  }

  current = gtk_list_box_row_get_header (row);
  if (!current) {
    current = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (current);
    gtk_list_box_row_set_header (row, current);
  }
}

static GtkWidget *
create_transaction (BankingTransaction *transaction)
{
  GtkWidget *child;
  GtkWidget *label;
  g_autofree gchar *tmp = NULL;

  child = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (child), 0);
  gtk_grid_set_column_spacing (GTK_GRID (child), 12);
  gtk_grid_set_row_homogeneous (GTK_GRID (child), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (child), 6);

  /* Set remote name */
  label = gtk_label_new ("");
  PangoAttrList *attrlist = pango_attr_list_new ();
  PangoAttribute *attr = pango_attr_weight_new (PANGO_WEIGHT_SEMIBOLD);
  pango_attr_list_insert (attrlist, attr);
  gtk_label_set_attributes (GTK_LABEL (label), attrlist);
  pango_attr_list_unref (attrlist);
  gtk_label_set_text (GTK_LABEL (label), banking_transaction_get_remote_name (transaction) ? banking_transaction_get_remote_name (transaction) : banking_transaction_get_transaction_text (transaction));
  gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_END);
  gtk_widget_set_hexpand (label, TRUE);
  gtk_label_set_xalign (GTK_LABEL (label), 0);
  gtk_widget_set_halign (label, GTK_ALIGN_START);
  gtk_grid_attach (GTK_GRID (child), label, 0, 0, 1, 1);

  /* Set purpose */
  tmp = g_strdup_printf ("<small>%s - %s</small>", banking_transaction_get_date (transaction), banking_transaction_get_purpose (transaction));
  GtkWidget *purpose = gtk_label_new (banking_transaction_get_purpose (transaction));
  gtk_label_set_markup (GTK_LABEL (purpose), tmp);
  gtk_label_set_ellipsize (GTK_LABEL (purpose), PANGO_ELLIPSIZE_END);
  gtk_widget_set_sensitive (purpose, FALSE);
  gtk_widget_set_halign (purpose, GTK_ALIGN_START);
  gtk_label_set_xalign (GTK_LABEL (purpose), 0);
  gtk_grid_attach (GTK_GRID (child), purpose, 0, 1, 1, 1);

  tmp = g_strdup_printf ("%.2f", banking_transaction_get_value (transaction));
  GtkWidget *value = gtk_label_new (tmp);
  attrlist = pango_attr_list_new ();
  if (banking_transaction_get_value (transaction) >= 0.0f) {
    attr = pango_attr_foreground_new (0, 40000, 0);
  } else if (banking_transaction_get_value (transaction) < 0.0f) {
    attr = pango_attr_foreground_new (40000, 0, 0);
  }
  pango_attr_list_insert (attrlist, attr);
  gtk_label_set_attributes (GTK_LABEL (value), attrlist);
  pango_attr_list_unref (attrlist);
  gtk_widget_set_halign (value, GTK_ALIGN_END);

  gtk_grid_attach (GTK_GRID (child), value, 1, 0, 1, 1);
  gtk_widget_show_all (child);

  return child;
}

static gboolean
add_transactions (BankingWindow *self)
{
  GtkWidget *transaction;
  GList *element;

  if (!self->transactions || !self->num_fetch) {
    self->sorter_source = 0;
    gtk_widget_queue_draw (self->transaction_listbox);

    return G_SOURCE_REMOVE;
  }

  element = self->transactions;

  transaction = create_transaction (element->data);
  gtk_list_box_insert (GTK_LIST_BOX (self->transaction_listbox), transaction, -1);

  self->transactions = g_list_remove_link (self->transactions, element);

  self->num_fetch--;

  if (!self->num_fetch) {
    self->sorter_source = 0;
    return G_SOURCE_REMOVE;
  }

  return G_SOURCE_CONTINUE;
}

static gboolean
load_details (gpointer user_data)
{
  BankingWindow *self = BANKING_WINDOW (user_data);

  self->transactions = banking_account_get_transactions (self->backend, self->account);
  self->transactions = g_list_reverse (self->transactions);

  clear_listbox (self->transaction_listbox);
  gtk_list_box_set_header_func (GTK_LIST_BOX (self->transaction_listbox), box_header_func, NULL, NULL);

  gtk_header_bar_set_title (GTK_HEADER_BAR (self->sub_header_bar), banking_account_get_name (self->account));
  gtk_header_bar_set_subtitle (GTK_HEADER_BAR (self->sub_header_bar), banking_account_get_number (self->account));

  hdy_leaflet_set_visible_child_name (self->content_box, "detail");

  update (self);

  self->num_fetch = NUM_FETCH_LIMIT;
  self->sorter_source = g_idle_add ((GSourceFunc)add_transactions, self);

  return G_SOURCE_REMOVE;
}

static void
remove_pending_transaction_source (BankingWindow *self)
{
  if (self->sorter_source != 0) {
    g_source_remove (self->sorter_source);
    self->sorter_source = 0;
  }
}

static void
on_edge_reached (GtkScrolledWindow *scrolled,
                 GtkPositionType    pos,
                 gpointer           user_data)
{
  BankingWindow *self = BANKING_WINDOW (user_data);

  if (pos == GTK_POS_BOTTOM) {
    remove_pending_transaction_source (self);

    self->num_fetch += NUM_FETCH_LIMIT;
    self->sorter_source = g_idle_add ((GSourceFunc)add_transactions, self);
  }
}

static void
on_row_selected (GtkListBox    *box,
                 GtkListBoxRow *row,
                 gpointer       user_data)
{
  BankingWindow *self = BANKING_WINDOW (user_data);

  self->account = g_object_get_data (G_OBJECT (gtk_bin_get_child (GTK_BIN (row))), "account");
  g_idle_add_full (G_PRIORITY_HIGH, load_details, self, NULL);
}

static void
banking_window_constructed (GObject *object)
{
  BankingWindow *self = BANKING_WINDOW (object);

  G_OBJECT_CLASS (banking_window_parent_class)->constructed (object);

  if (banking_backend_get_num_accounts (self->backend))
    g_idle_add (load_accounts, self);
}

static void
banking_window_class_init (BankingWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = banking_window_constructed;
  object_class->set_property = banking_window_set_property;
  object_class->get_property = banking_window_get_property;

  obj_properties[PROP_BACKEND] =
    g_param_spec_object ("backend",
                         "Banking Backend",
                         "Banking Backend",
                         G_TYPE_OBJECT,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, LAST_PROP, obj_properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/banking/ui/banking-window.ui");
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, header_box);
  //gtk_widget_class_bind_template_child (widget_class, BankingWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, header_group);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, sub_header_bar);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, content_box);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, back_button);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, account_listbox);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, transaction_listbox);
  /*gtk_widget_class_bind_template_child (widget_class, BankingWindow, bank_entry);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, bank_completion);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, account_number);
  gtk_widget_class_bind_template_child (widget_class, BankingWindow, account_password_entry);*/

  gtk_widget_class_bind_template_callback_full (widget_class, "on_notify_fold", G_CALLBACK (on_notify_fold));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_notify_visible_child", G_CALLBACK (on_notify_visible_child));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_notify_header_visible_child", G_CALLBACK (on_notify_header_visible_child));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_back_button_clicked", G_CALLBACK (on_back_button_clicked));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_login_button_clicked", G_CALLBACK (on_login_button_clicked));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_edge_reached", G_CALLBACK (on_edge_reached));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_row_selected", G_CALLBACK (on_row_selected));
}

static void
banking_window_init (BankingWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->user_name = g_strdup (g_get_real_name ());
  gtk_list_box_set_header_func (GTK_LIST_BOX (self->account_listbox), box_header_func, NULL, NULL);
  gtk_list_box_set_header_func (GTK_LIST_BOX (self->transaction_listbox), box_header_func, NULL, NULL);
}

static void
dialog_close_cb (GtkDialog *dialog,
                 gint       response_id,
                 gpointer   user_data)
{
  BankingWindow *self = BANKING_WINDOW (user_data);

  if (response_id == GTK_RESPONSE_ACCEPT) {
    self->user = banking_backend_create_user (self->backend,
                                              gtk_entry_get_text (GTK_ENTRY (self->bank_entry)),
                                              gtk_entry_get_text (GTK_ENTRY (self->account_number)));

    banking_user_get_sysid (self->backend, self->user);
    banking_user_get_accounts (self->backend, self->user);
    banking_user_load_balance (self->backend, self->user);

    g_idle_add (load_accounts, dialog);
  }

  gtk_widget_destroy (GTK_WIDGET (self));
}

void
banking_window_show_assistant (BankingWindow *self)
{
  banking_assistant_new (self, self->backend);
}

void
banking_window_show_about (BankingWindow *self)
{
  GtkWidget *about_dialog;
  GtkWidget *box;
  GtkWidget *image;
  GtkWidget *title;
  GtkWidget *sub_title;
  PangoAttrList *attrlist;
  PangoAttribute *attr;

  about_dialog = hdy_dialog_new (GTK_WINDOW (self));
  gtk_window_set_title (GTK_WINDOW (about_dialog), "About");
  g_signal_connect (G_OBJECT (about_dialog), "response", G_CALLBACK (dialog_close_cb), NULL);

  /* Box */
  box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 6);
  gtk_widget_set_vexpand (box, TRUE);
  gtk_widget_set_valign (box, GTK_ALIGN_CENTER);
  gtk_widget_set_margin_top (box, 12);
  gtk_widget_set_margin_bottom (box, 12);

  /* Image */
  image = gtk_image_new_from_resource ("/org/tabos/banking/banking.png");
  gtk_widget_show (image);
  gtk_box_pack_start (GTK_BOX (box), image, TRUE, TRUE, 6);

  /* Title */
  title = gtk_label_new ("Banking for GNOME");
  gtk_widget_show (title);
  gtk_widget_set_margin_top (title, 22);

  attrlist = pango_attr_list_new ();
  attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
  pango_attr_list_insert (attrlist, attr);
  attr = pango_attr_scale_new (2.0f);
  pango_attr_list_insert (attrlist, attr);
  gtk_label_set_attributes (GTK_LABEL (title), attrlist);
  pango_attr_list_unref (attrlist);

  gtk_box_pack_start (GTK_BOX (box), title, TRUE, TRUE, 6);

  /* Sub title */
  sub_title = gtk_label_new ("Created by Jan-Michael Brummer");
  gtk_widget_show (sub_title);
  gtk_box_pack_start (GTK_BOX (box), sub_title, TRUE, TRUE, 6);

  gtk_container_add (GTK_CONTAINER (gtk_dialog_get_content_area (GTK_DIALOG (about_dialog))), box);

  gtk_widget_show (box);
  gtk_widget_show (about_dialog);
}

void
banking_window_refresh (BankingWindow *self)
{
  banking_user_load_balance (self->backend, self->user);
}
